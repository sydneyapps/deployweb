<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Downloadable extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    
    protected $uiName ='';

    protected $urlName ='';

    /**
    * The constructor
    * @name path to the file with in the form /public/foldername/filename
    */
    public function __construct($name){

        $frag = explode('/', $name);

        // Gets the folder name located in /storage/app/public/
        if(count($frag) > 1){
            $this->urlName = $frag[count($frag)-2];
        }
        $this->uiName = $frag[count($frag)-1];
    }

    public function getUIName(){

        $name = explode('.',(ucfirst(str_replace('-',' ', $this->uiName))));
        return $name[0];
    }

    public function getUrl(){

        if($this->urlName == ''){
            return $this->uiName();
        }
        return $this->urlName.'/'.$this->uiName;
    }

}