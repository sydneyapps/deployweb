<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use App\Http\Model;

/**
 * Description of GenericBaseResponse
 *
 * @author Sydney
 */
class GenericBaseResponse extends BaseResponse {
    //put your code here
    
    public function __construct($status, $message, $data) {
        parent::__construct($status, $message);
        $this->data = $data;
    }
    public function setData($data) {
        return parent::setData($data);
    }

}
