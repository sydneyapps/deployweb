<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of ContactFormResponse
 *
 * @author Sydney
 */
class ContactFormResponse {
    //put your code here
    
    public $element, $error;
    public function __construct($element, $error) {
        $this->element = $element;
        $this->error = $error;
    }

    public function getElement() {
        return $this->element;
    }

    public function getError() {
        return $this->error;
    }

    public function setElement($element) {
        $this->element = $element;
        return $this;
    }

    public function setError($error) {
        $this->error = $error;
        return $this;
    }
}
