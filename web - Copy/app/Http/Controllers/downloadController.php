<?php

namespace App\Http\Controllers;

use App\Model\Downloadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class downloadController extends Controller
{
 
    const DOWNLOADS_VIEW_FOLDER = "downloads";

    public function download($dname, $fname){
        
        $files = Storage::allFiles('/public/'.$dname);

        for($x = 0; $x < count($files); $x++) { 

            $downloads = substr($files[$x], - strlen($fname));
            if($downloads == $fname)
            {
                    return response()->download(storage_path("app/public/$dname/$fname"));
            }

        }

    }

    public function index($downloads){
        return $this->getView($downloads);
    }

    private function getView( $dnload){
        $files = Storage::allFiles('/public/'.$dnload);

        $downloads = array();
        for($x = 0; $x < count($files); $x++) {        
            echo($files[$x]);  
            $downloads[$x] = new Downloadable( $files[$x]);
        }
        return view(self::DOWNLOADS_VIEW_FOLDER.'.'.$dnload, ['files'=>$downloads]);

        
    }

}