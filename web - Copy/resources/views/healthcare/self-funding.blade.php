@extends('layouts.page')
@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Self Funding
@endsection
@section('article-body')
<p>Should your capital exceed the Upper Capital threshold (£23,250 -England 2010/11) you will need to pay for your own care which could either be provided by the Local Authority or more likely by private carers.</p>
<p>Lifeway’s Community Care recognises the role of families and carers in helping those who use our services to choose the best support and care package available.</p>
<h4>Benefits team<strong><em></em></strong></h4>
<p><strong><em></em></strong>We appreciate how difficult it can be for people to find their way around the benefits system, which can be extremely complex. As a result, we have created a dedicated in-house Benefits Team who can help people apply for DWP benefits, Housing Benefit and other available income streams. We can also advise on Individual Budgets, Personal Budgets and <strong>Direct Payments</strong>.</p>
<p>We work closely with Social Services and NHS Primary Care Trusts as well as Supported Living teams, in order to consolidate the funding arrangements for Purchasers and Commissioners.</p>
<h4>Housing needs<strong><em></em></strong></h4>
<p><strong><em></em></strong>We also work closely with housing providers in order to ensure the best accommodation is provided for those who use our services and, in line with good practice, keep the role of housing and social care completely separate.</p>
<p>When it comes to making a house a home, we are always delighted to help. Whether family members or carers are helping to choose paint colours or furniture, or require other levels of advice and assistance, our aim is to work with everyone who uses our services to ensure their needs, as outlined in their person-centred plan, are met.</p>
@endsection