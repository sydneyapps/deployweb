@extends('layouts.page')

@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Personal Care
@endsection
@section('article-body')				
								<article id="post-94" class="post-94 page type-page status-publish hentry">
									<header class="entry-header">
										<h4 class="entry-title">Housekeeping Services</h4>
									</header><!-- .entry-header -->
									<div class="entry-content">
										<p>Sometimes&nbsp;just a bit of&nbsp;help with cooking and cleaning, shopping or preparing meals can make all the difference, particularly if you’ve&nbsp;been ill or feeling under the weather.&nbsp; As with all our services, we are totally flexible&nbsp;and will fit in to&nbsp;suit your exact needs.</p>
										<p>Housekeeping covers such things as:</p>
										<ul>
										<li>Making or changing beds</li>
										<li>Writing out shopping lists</li>
										<li>Going shopping or accompanying&nbsp;on&nbsp;shopping trip</li>
										<li>Paying Bills/Collecting Pensions</li>
										<li>Assistance with laundry/ironing</li>
										<li>Vacuum cleaning &amp; other light domestic tasks</li>
										<li>Washing dishes</li>
										<li>Cleaning&nbsp;toilets and bathrooms</li>
										<li>Dog walking</li>
										</ul>
										<p>To find out more about our Companionship services, please contact us on our free telephone <b>0800 978 8434</b> or email us <a href="mailto:care@divinemotions.co.uk"><em><b>care@divinemotions.co.uk</b></em></a></p>
									</div>
								</article>
    @endsection