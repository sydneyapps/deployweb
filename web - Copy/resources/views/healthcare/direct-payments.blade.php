@extends('layouts.page')
@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Direct Payments
@endsection
@section('article-body')
<p>They are cash payments given to service users in lieu of community care services they have been assessed as needing, and are intended to give users greater choice in their care. The payment must be sufficient to enable the service user to purchase services to meet their needs, and must be spent on services that meet eligible needs.</p>
<p>They confer responsibilities on recipients to employ people, often known as personal assistants, or to commission services for themselves. Service users can get support in fulfilling these responsibilities from direct payment support services commissioned by local authorities, often from user-led organisations.</p>
<p>Direct payments are available across the UK and to all client groups, including carers, disabled children and people who lack mental capacity. However, they cannot be used to purchase residential care or services provided directly by local authorities.</p>
<p>To find out more information about the support services we provide for Direct Payment Service Users and<strong> Personal Assistants</strong> please contact us on our free telephone <b>0800 978 8434</b> or email us <b>care@divinemotions.co.uk</b></p>
@endsection