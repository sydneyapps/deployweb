@extends('layouts.page')
@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Sleeping Nights
@endsection
@section('article-body')
					
<article class="">
<p>Night care might be required&nbsp;to provide&nbsp; a break for your family or primary carer or for a short duration while you recover from an accident, illness or hospital stay.&nbsp; As with all our services, we can adapt&nbsp;to meet your specific needs.</p>
<p>Waking Nights or&nbsp;Sleepovers: one of our care staff will come to your home for the night hours&nbsp;and remain awake to provide whatever assistance or monitoring is required.</p>
<p>Non-waking or Sleeping Nights –&nbsp;one of our care staff will sleep in your home&nbsp;during the night hours to provide peace of mind and assistance if required.</p>
</article>
@endsection