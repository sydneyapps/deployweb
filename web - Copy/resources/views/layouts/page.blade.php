@extends('layouts.base')
@section('page-title')
@yield('title')
@endsection
@section('content')
<section id="intro" class="intro">
		<div class="intro-content intro-nobg">
			<div class="container">

				<div class="row">
					
					<div class="col-lg-12">
						<div class="home-section">
							<h2 class="h-bold">
						@yield('article-title')
							</h2>
						</div>
					</div>
					<div class="col-md-12 paddingtop-40">
						<div id="main-display">
							<div class="entry-content" role="main">
								@yield('article-body')
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
</section>
@endsection