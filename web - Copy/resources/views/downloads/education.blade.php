@extends('layouts.download-page')

@section('page-title')
Divine Motions Education
@endsection

@section('article-title')
	Education 
@endsection
@section('article-body')
            @foreach($files as $file) 
					<div class="col-md-12  paddingtop-40">
						<div class="callaction bg-gray">
							<div class="row">
								<div class="col-md-8">
									<div class="wow fadeInUp" data-wow-delay="0.1s">
										<div class="cta-text">
											<h3>Brochure</h3>
											<p>We provide career oriented training </p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="wow lightSpeedIn" data-wow-delay="0.1s">
										<div class="cta-btn">
											<a target="blank" href="/download/{{ $file->getUrl() }}" class="btn btn-skin btn-lg">Download the {{ $file->getUIName() }} file</a>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
            @endforeach
    @endsection