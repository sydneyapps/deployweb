@extends('layouts.page')
@section('page-title')
Divine Motions Aca Care : Supported Living - Outreach support
@endsection
@section('article-title')
Supported Living : Outreach support
@endsection
@section('article-body')
<article>
<div class="entry-content">

<p>Outreach support can be for any number of hours a week and can be used to give family carers a break from caring for a loved one. It also gives people a chance to have a change of scene with another familiar face. It’s an excellent way for people with learning disabilities to try something new and to develop their social skills, independence and self-confidence.</p>
<p>We provide outreach, flexible support services, our delivery model is person centered, tailor made to meet range of support plan needs at different times. </p>
<p>Our Individuals use our outreach services are for specific daily skills such as, healthy meal preparation, budgeting  while shopping, travel while using public transport, attending  preferred social and community  activities, social networking clubs, doing activities such as swimming, going to work or attending  going to college. Time is planned that is, agreed while flexibility is available.  </p>
<p><strong>We provide skills for job search, training and build confidence.</strong><br>
Divine Motions value and believe in the importance of life-long learning we make use of all that the local community is offering the service users. We identify learning needs that is, whether a person wants to learn a new skill or find a paid or voluntary job, outreach support can help them to make this happen through key worker support. We help people to attend college courses, to find or attend work placements, to travel to and from work independently and to gain paid or voluntary work.</p>

<p><strong>Having a break</strong><br>
Key workers support service users to go on holiday, this can mean a short break or the overseas holiday depending on preferences. Matching support workers to support service users going on holiday. Usually these are support workers who share the same interests and are skilled in supporting them. We’ve enabled people to go and visit the beloved ones. Some services users have gone on cruise trips like Disneyland Florida, others have gone for camping holidays and music and theatre-themed weekends</p>
</div>
</article>
@endsection