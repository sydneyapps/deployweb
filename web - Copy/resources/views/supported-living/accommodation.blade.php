@extends('layouts.page')
@section('page-title')
Divine Motions Aca Care : Supported Living - Accommodation
@endsection
@section('article-title')
Supported Living : Accommodation
@endsection
@section('article-body')
<article >

<div class="entry-content">
		<p>Divine Motions access accommodation from Landlords and we maintain the properties up to housing safe standards (HMO). Our units are centrally located and are close to amenities, shops and public transport.</p>
<button class="accordion"><strong>Our housing facilities are:</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<ul>
<li>Self -contained studio / or en-suite.</li>
<li>8 double bedrooms with en-suite in each room.</li>
<li>Each room has a communal living room</li>
<li>Each room has its own kitchen</li>
<li>Large kitchen</li>
<li>Dining room ( communal)</li>
<li>Good size back garden with pavement</li>
<li>Double glazing throughout</li>
<li>Gas Central Heating /glazed windows</li>
<li>Laminate floors in bedrooms</li>
</ul>
</div>
<button class="accordion"><strong>In-Room Amenities:</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<ul>
<li>As standard each room has a free Sky TV</li>
<li>Each room has private telephone</li>
<li>Each has free internet access</li>
<li>Some bedrooms have key codes.</li>
<li>Four activity rooms available with excellent facilities.</li>
</ul>
</div>
<button class="accordion"><strong>Quality assurance requirement for occupation legibility is available which include:</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<ul>
<li>Energy Performance Certificate (EPC)</li>
<li>House with Multiple Occupants licence (C2) licenses</li>
<li>Gas Safety Certificate and Electric Safety ( is available)</li>
<li>These units are staffed 24 hours a day</li>
<li>Monitored 24 hours a day by CCTV cameras. (Not in bedrooms due to (DoLs)</li>
</ul>
<p>The property is available for immediate placement occupation and ideal for 24hours services or emergency temporary accommodation.</p>
</div>
</article>
@endsection