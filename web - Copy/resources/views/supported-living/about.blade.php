@extends('layouts.page')
@section('page-title')
Divine Motions Aca Care : Supported Living - about
@endsection
@section('article-title')
Supported Living : About
@endsection
@section('article-body')
<article>
<button class="accordion" ><strong>WHO WE SUPPORT
</strong><i class="fa fa-plus"></i></button>
<div class="panel">We support people individually or in small friendship groups to enable them to live their lives as independently as possible. Our services include support for people who manage with very few hours of support each week and people with higher level needs including people who require continuous staffing from support workers.</p>
<ul>
<li>Young and Elderly people</li>
<li>People with physical disabilities</li>
<li>People with learning disabilities</li>
<li>Mental illness</li>
<li>People who require palliative care</li>
<li>People recently out of hospital and are</li>
<li>recovering after a long illness.</li>
<li>Older people with a variety of needs</li>
</ul>
</div>
<button class="accordion" ><strong>AREAS OF SUPPORT
</strong><i class="fa fa-plus"></i></button>
<div class="panel">
We are committed to support people to live independently in<br>
the comfort of their own home. We aim to support each individual such that they remain as independent as possible. Support is provided in these areas:</p>
<ul>
<li>Domestic Services</li>
<li>Shopping</li>
<li>Cleaning your home</li>
<li>Activities and opportunities for socializing</li>
<li>Engage in sport and recreation</li>
<li>Money and Bills</li>
<li>Tenancy Support</li>
<li>24hr Support (Sleep-In or Waking Night)</li>
<li>Holiday Support</li>
<li>Skills for Living</li>
<li>Cooking</li>
<li>Holiday Support</li>
</ul>
<p>Service users who require Personal Care, we work in partnership<br>
with regulated organization to provides these services.</p>
<p>Service users who require Personal Care, we work in partnership with regulated organization to provides these services.As with all our support, we develop person-centred support plans and involve support them to achieve their goals and aspirations. Through individual support contracts, where appropriate, and supported living guidance, we agree with people what support will and will not include, enabling.</p>
</div>
<button class="accordion" ><strong>OUR QUALITY</strong><i class="fa fa-plus"></i></button>
<div class="panel">The views of our Service Users, whether complementary or critical, are very important to us, and are invaluable in helping us to constantly evaluate and improve our care service quality. </p>
<p>We aim to provide a comprehensive range of professional high quality care and support services that are flexible, responsive and dedicated to meeting the needs of our Service Users and their carers. We understand everyone who chooses our service is different. We aim to asses, plan and deliver your care with a clear focus on your needs and abilities. </p>
<p>By tailoring our services to meet your needs, we aim to maintain your personal independence and dignity and enable you to live the kind of life you want to live.</p>
</div>
@endsection