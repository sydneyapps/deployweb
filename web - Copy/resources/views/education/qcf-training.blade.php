@extends('layouts.page')
@section('page-title')
Divine Motions Aca Care : Education - QCF Training
@endsection
@section('article-title')
Supported Living : Accommodation
        QCF Training
@endsection
@section('article-body')
<article>

<p>Do you have an interest in Health and Social Care? Is&nbsp;your goal to work as a nurse, social worker, health visitor&nbsp;or even as a paramedic? Then these courses below can&nbsp;provide you with the right stepping stone Do you have an&nbsp;interest in Health and Social Care? Is your goal to work as&nbsp;a nurse, social worker, health visitor or even as a&nbsp;paramedic? Then this course can provide you with the&nbsp;right stepping stone. Our dedicated team will discuss with&nbsp;you your chosen course in detail.</p>

<button class="accordion"><strong>BTEC Level 1 Diploma in Health and Social Care (16-24)</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<p>This is the ideal course for those wishing to progress into a career in the Health, Care or Science related fields. The course studies a variety of subjects including diet and health, human lifespan development and units directly related to health care industry. It is ideal for those wanting to work within the Health and Care Industry as it allows you to progress onto higher level courses or directly into workplace.<br></p>
</div>

<button class="accordion"><strong>Level 2 Diploma in Health and Social Care</strong><i class="fa fa-plus"></i></button>

<div class="panel">
<p>The course has various units with three pathways:<br>
<strong>Generic Health and Social Care pathway:</strong></p>
<p><em>Dementia</em></p>
<p><em>Pathway and Learning Disability pathway.</em></p>
<p>You will&nbsp;be required to achieve at least 46 credits to complete the qualification. If you are working in care – funding is available to eligible<br>
persons.</p>

</div>

<button class="accordion"><strong>Level 3 Diploma in Health and Social Care</strong><i class="fa fa-plus"></i></button>
<div class="panel">

<p><strong>The course has various units three pathways:</strong></p>
<p><em>Generic Health and Social Care pathway</em></p>
<p><em>Dementia pathway</em></p>
<p><em>and Learning Disability pathway</em></p>
<p>It also contains units from the Level 2&nbsp;Diploma in Health and Social Care (Adults) for&nbsp;England. If learners have completed the&nbsp;Level 2&nbsp;qualification they do not need to repeat the shared&nbsp;units already achieved.</p>
<h2>Level 1 Award in Principles of &nbsp;Customer Services (QCF)</h2>
<p>The Level 1 Award in Priinciples in Customer Services is designed to allow learners to learn, develop and practise the skills and knowledge required for employment and/or a career within the customer service sector, spanning all sectors of industry and commerce, from call centres and banks or hair salons.</p>
<p>This Qualification covers skills and knowledge required by a learner to deliver efficient and reliable customer services through an understanding of:<br>
customer expectations and needs<br>
the delivery of customer services in line with organistaional procedures<br>
dealing effectivley with customer quieries, problems and complaints.</p>
<p><strong>Leadership for Health and Social Care and Childrens and Young People’s Services (Adult’s Management) (England) Level 5 Diploma </strong></p>
<p>This qualification is best suited to those who are already working in a management role in a care home, nursing home or in a residential care setting for adults; or those who are responsible for planning services, measuring performance and delivering continuous improvement in care services.</p>

</div>

<button class="accordion"><strong>Basic skills training</strong><i class="fa fa-plus"></i></button>

<div class="panel">
<p><strong></strong></p><div class="wpcol-one-half"><p></p>
<p>Induction Awareness Training<br>
Moving and Handling<br>
Infection Control<br>
Hand Hygiene<br>
Handling Medicines<br>
</p></div><br>
Health &amp; Safety<br>
Adult Protection (SOVA)<br>
Food and Hygiene<br>
Dementia Care<br>
Medication Administration<br>
</div>
</article>
@endsection