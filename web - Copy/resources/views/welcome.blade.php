
@extends('layouts.home')

@section('page-title')
Home - Divine Motions Aca Care
@endsection

@section('home-content')
@section('slider-bar-top')
    @parent

    <div class="col-lg-12">
		<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
			<h2 class="h-ultra">Welcome to our website</h2>
		</div>
		<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
			<h4 class="h-light">Providing quality healthcare services</h4>
		</div>

		
        <div class="well well-trans">
			<div class="wow fadeInRight" data-wow-delay="0.1s">
<!--
				<ul class="lead-list">
					<li><span class="fa fa-wheelchair fa-2x icon-link"></span> <span class="list"><strong>Affordable healthcare packages</strong><br />Benefit from our highly trained healthcare proffessionals</span></li>
					<li><span class="fa fa-edit fa-2x icon-link"></span> <span class="list"><strong>Apply for one of the available positions</strong><br />We are recruiting health care workers for various positions</span></li>
					<li><span class="fa fa-book fa-2x icon-link"></span> <span class="list"><strong>Enrol with our health care academy</strong><br />Pursue a career in health care through our training programs</span></li>
				</ul>

				<p class="text-right wow bounceIn" data-wow-delay="0.4s">
					<a href="#" class="btn btn-skin btn-lg">Learn more <i class="fa fa-angle-right"></i></a>
				</p> -->

				<audio id="prompts" autoplay="false" controls  src="/assets/media/acacareprompts.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
				</audio>

				<div id="prompts-slider" class="lead-list">
					<div class="slider-container" >
						<img src="/img/prompts/01.jpg" class="active"/>
						<img src="/img/prompts/02.jpg" />
						<img src="/img/prompts/03.jpg" />
					</div>
				</div>
			</div>
        </div>
    </div>
	<!--
    <div class="col-lg-6">
		<audio id="prompts" autoplay="true" controls>
			<source src="/assets/media/acacareprompts.mp3" type="audio/ogg">
			<source src="/assets/media/acacareprompts.mp3" type="audio/mpeg">
				Your browser does not support the audio element.
		</audio>

		<div id="prompts-slider">
			<img src="/img/prompts/01.jpg" />
		</div>
		
    </div>
-->

@endsection
@section('slider-bar-bottom')
    @parent
    @endsection

	<!-- Section: boxes -->
    <section id="boxes" class="home-section paddingtop-80">
	
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="wow fadeInUp" data-wow-delay="0.2s">
						<div class="box text-center">
							
							<i class="fa fa-check fa-3x circled bg-skin"></i>
							<h4 class="h-bold">Make an appoinment</h4>
							<p></p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="wow fadeInUp" data-wow-delay="0.2s">
						<div class="box text-center">
							
							<i class="fa fa-list-alt fa-3x circled bg-skin"></i>
							<h4 class="h-bold">Education</h4>
							<p>
							Why study at Divine Motions Innovative and relevant Career focused Excellence in teaching 
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="wow fadeInUp" data-wow-delay="0.2s">
						<div class="box text-center">
							<i class="fa fa-user-md fa-3x circled bg-skin"></i>
							<h4 class="h-bold">Recruitement</h4>
							<p>Apply online or download the application form.
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="wow fadeInUp" data-wow-delay="0.2s">
						<div class="box text-center">
							
							<i class="fa fa-hospital-o fa-3x circled bg-skin"></i>
							<h4 class="h-bold">Care Services</h4>
							<p>
							Find out more about our healthcare services.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
	<!-- /Section: boxes -->
	
	
	<section id="callaction" class="home-section paddingtop-40">	
           <div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="callaction bg-gray">
							<div class="row">
								<div class="col-md-8">
									<div class="wow fadeInUp" data-wow-delay="0.1s">
									<div class="cta-text">
									<h3>Looking for work?</h3>
									<p>We are recruiting healthcare assistants, nurses and carers </p>
									</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="wow lightSpeedIn" data-wow-delay="0.1s">
										<div class="cta-btn">
										<a href="#" class="btn btn-skin btn-lg">Apply Today</a>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
	</section>	
	

	<!-- Section: services -->
    <section id="service" class="home-section nopadding paddingtop-60">

		<div class="container">

        <div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
				<img src="img/dummy/img-1.jpg" class="img-responsive" alt="" />
				</div>
            </div>
			<div class="col-sm-3 col-md-3">
				
				<div class="wow fadeInRight" data-wow-delay="0.1s">
                <div class="service-box">
					<div class="service-icon">
						<span class="fa fa-user fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light">Mental Health</h5>
						<p>Support for severe and enduring mental health needs.</p>
					</div>
                </div>
				</div>
				
				<div class="wow fadeInRight" data-wow-delay="0.2s">
				<div class="service-box">
					<div class="service-icon">
						<span class="fa fa-wheelchair fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light">Companionship / Sitting services</h5>
						<p>Someone to keep you company now and again</p>
					</div>
                </div>
				</div>
				<div class="wow fadeInRight" data-wow-delay="0.3s">
				<div class="service-box">
					<div class="service-icon">
						<span class="fa fa-plus-square fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light">Supported living</h5>
						<p>Person centred services to people with a learning disability.</p>
					</div>
                </div>
				</div>


            </div>
			<div class="col-sm-3 col-md-3">
				
				<div class="wow fadeInRight" data-wow-delay="0.1s">
                <div class="service-box">
					<div class="service-icon">
						<span class="fa fa-h-square fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light">Respite Care</h5>
						<p>Your family member is well cared while you take a break.</p>
					</div>
                </div>
				</div>
				
				<div class="wow fadeInRight" data-wow-delay="0.2s">
				<div class="service-box">
					<div class="service-icon">
						<span class="fa fa-filter fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light" >Learing disabilities.</h5>
						<p>Service users range from low to complex dependent.</p>
					</div>
                </div>
				</div>
				<div class="wow fadeInRight" data-wow-delay="0.3s">
				<div class="service-box">
					<div class="service-icon">
						<span class="fa fa-building fa-3x"></span> 
					</div>
					<div class="service-desc">
						<h5 class="h-light">Live in </h5>
						<p>An affordable alternative to residential or care home.</p>
					</div>
                </div>
				</div>

            </div>
			
        </div>		
		</div>
	</section>
	<!-- /Section: services -->

@endsection

@section('scripts')
<script src="/js/prompts.js?v=0.1"></script>
@endsection