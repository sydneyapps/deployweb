@extends('layouts.page')

@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
General Information
@endsection
@section('article-body')
								<p>
								We are a registered Domiciliary Care provider, offering Community Supported Living to people living in the community, either with their families or in their own homes.
								</p>
								<p>
								We offer support with personal care, as well as help and support for each person to learn new skills, develop independence, access other services and enjoy life as part of the community.
								</p>
								<p>
									We provide home based support to individuals who are referred by Social Services or who are purchasing services through direct payment. or Individual Budget.
									Each person has a Support Plan and an Activity Planner. Information includes personal care, learning opportunities, recreational, social, respite breaks, vocational and work activities.
								</p>
									We provide care and support services to the following Service User groups
								</p>
								<p>
									Mental Health
									Elderly people
									People with physical disabilities
									People with learning disabilities
									People who require palliative care
									People recently out of hospital and are recovering after a long illness.
								</p>
@endsection