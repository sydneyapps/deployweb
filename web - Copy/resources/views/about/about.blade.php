@extends('layouts.intro-page')

@section('page-title')
About - Divine Motions Aca Care
@endsection

	@section('slider-bar-top')
    	@parent
    <div class="col-lg-6">
    <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
    <h2 class="h-ultra">About Us</h2>
    </div>
    <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
    <h4 class="h-light">A registered Domiciliary Care Provider</h4>
    </div>
        <div class="well well-trans">
        <div class="wow fadeInRight" data-wow-delay="0.1s">
        <ul class="lead-list">
		<li><span class="fa fa-book fa-2x icon-link"></span> <span class="list"><strong>CQC Report</strong><br />We are regulated by the CQC </span></li>
		<li><span class="fa fa-book fa-2x icon-link"></span> <span class="list"><strong>Policies</strong><br /></span></li>
		<li><span class="fa fa-book fa-2x icon-link"></span> <span class="list"><strong>General Information and advice</strong><br /></span></li>
		</ul>
        </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
        <img src="img/dummy/img-1.png" class="img-responsive" alt="" />
        </div>
    </div>
	@endsection
	
	@section('slider-bar-bottom')
    	@parent
    @endsection
	
	@section('home-content')
    <section id="partner" class="home-section">	
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="home-section">
						<h2 class="h-bold">Who we are </h2>
					<p>
						We are a registered Domiciliary Care provider, offering Community Supported Living to people living in the community, either with their families or in their own homes.
						</p><p>
						We offer support with personal care, as well as help and support for each person to learn new skills, develop independence, access other services and enjoy life as part of the community.
						</p>
						<p>
						We provide home based support to individuals who are referred by Social Services or who are purchasing services through direct payment. or Individual Budget.
						</p>
						<p>
						Each person has a Support Plan and an Activity Planner. Information includes personal care, learning opportunities, recreational, social, respite breaks, vocational and work activities.
						</p>
						<p>
						We provide care and support services to the following Service User groups
						</p>
						<p>
						Mental Health
						Elderly people
						People with physical disabilities
						People with learning disabilities
						People who require palliative care
						People recently out of hospital and are recovering after a long illness.
					</p>
					</div>
				</div>
			</div>
		</div>
    </section>
    @endsection


